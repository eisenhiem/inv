<?php

namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Items;
use app\models\ItemsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\ItemType;
use app\models\Repairs;
use app\models\RepairsSearch;
use app\models\Check;
use app\models\Sign;
use app\models\CheckSearch;
use app\models\Move;
use app\models\MoveSearch;

use kartik\mpdf\Pdf;
use Da\QrCode\QrCode;

/**
 * ItemsController implements the CRUD actions for Items model.
 */
class ItemsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Items models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Items model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Items model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Items();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $model->PICTURE = $model->upload($model,'PICTURE');

            $urlpath = Url::toRoute(['items/view','id'=> $model->ITEM_ID], true);
            $filepath = '../web/qrcode/';
            $filelocation = $filepath.'code'.$model->ITEM_ID.'.png';
            $model->PHOTO_FILE = $filelocation;
            $model->save(false);

            $filepath = '../web/qrcode/';
            $qrCode = (new QrCode($urlpath))
                ->useForegroundColor(0, 0, 0)
                ->useBackgroundColor(255, 255, 255)
                ->useEncoding('UTF-8')
            //  ->setErrorCorrectionLevel(ErrorCorrectionLevelInterface::HIGH)
            //  ->setLogoWidth(60)
                ->setSize(300)
                ->setMargin(5)
                ->setLabel('Product No:'.$model->ITEM_NO);
            $qrCode->writeFile($filepath.'code'.$model->ITEM_ID.'.png');
            return $this->redirect(['view', 'id' => $model->ITEM_ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionGenqr($id){
        
        $model = $this->findModel($id);

        $filepath = '../web/qrcode/';
        $filelocation = $filepath.'code'.$model->ITEM_ID.'.png';
        $model->PHOTO_FILE = $filelocation;
        $model->save();
        $urlpath = Url::toRoute(['items/view','id'=> $model->ITEM_ID], true);
        $qrCode = (new QrCode($urlpath))
            ->useForegroundColor(0, 0, 0)
            ->useBackgroundColor(255, 255, 255)
            ->useEncoding('UTF-8')
        //  ->setErrorCorrectionLevel(ErrorCorrectionLevelInterface::HIGH)
        //  ->setLogoWidth(60)
            ->setSize(300)
            ->setMargin(5)
            ->setLabel('Product No:'.$model->ITEM_NO);
        $qrCode->writeFile($filepath.'code'.$model->ITEM_ID.'.png');
        return $this->redirect(['view', 'id' => $model->ITEM_ID]);

    }

    /**
     * Updates an existing Items model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->PICTURE = $model->upload($model,'PICTURE');
            $model->save();
            return $this->redirect(['view', 'id' => $model->ITEM_ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Items model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Create repair item a single Items model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRepair($id)
    {
        $model = $this->findModel($id);
    
        return $this->redirect(['repairs/create', 'id' => $model->ITEM_ID]);
    }

    /**
     * Displays repair history a single Items model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionHistory($id)
    {
        $model = $this->findModel($id);

        $searchModel = new RepairsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where('ITEM_ID = '.$id);
    
        return $this->render('history', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTransfer($id)
    {
        $model = $this->findModel($id);
        
        $searchModel = new MoveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where('ITEM_ID = '.$id);
    
        return $this->render('transfer', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Create check list a single Items model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCheck($id)
    {
        $model = $this->findModel($id);
    
        return $this->redirect(['check/create', 'id' => $model->ITEM_ID]);
    }

    /**
     * Create check list a single Items model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionMove($id)
    {
        $model = $this->findModel($id);
    
        return $this->redirect(['move/create', 'id' => $model->ITEM_ID, 'location' => $model->LOCATION_ID]);
    }

    /**
     * Displays check list history a single Items model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionChecklist($id)
    {
        $model = $this->findModel($id);

        $searchModel = new CheckSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where('ITEM_ID = '.$id);
    
        return $this->render('checklist', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all items models.
     * @return mixed
     */
    public function actionPrintrepair($id)
    {
        $model = $this->findModel($id);

        $searchModel = new RepairsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where('ITEM_ID = '.$id);

        $content = $this->renderPartial('_history', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'รายชื่ออุปกรณ์'],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

            // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

    /**
     * Lists all items models.
     * @return mixed
     */
    public function actionPrintcheck($id)
    {
        $model = $this->findModel($id);

        $searchModel = new CheckSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where('ITEM_ID = '.$id);

        $content = $this->renderPartial('_checklist', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'รายชื่ออุปกรณ์'],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

            // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

      /**
     * Lists all items models.
     * @return mixed
     */
    public function actionPrintlabel($id)
    {
        $model = $this->findModel($id);
        $data = Sign::findOne(['ID'=>1]);

        $searchModel = new ItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where('ITEM_ID = '.$id);

        $content = $this->renderPartial('_label', [
            'model' => $model,
            'data' => $data,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => [100,64],
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'รายชื่ออุปกรณ์'],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ],
            'marginLeft' => 5, 
            'marginTop' => 5, 
            'marginRight' => 5, 
            'marginBottom' => 5,
        ]);

            // return the pdf output as per the destination setting
        return $pdf->render(); 
    }
  
    /**
     * Finds the Items model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Items the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Items::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}
