<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sign */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sign-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'HOSPITAL_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'HOSP_NO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DIRECTOR_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DIRECTOR_POSITION1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DIRECTOR_POSITION2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MANAGE_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MANAGE_POSITION')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'IT_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'IT_POSITION')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
