<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RepairStatus */

$this->title = 'เพิ่มสถานะการส่งซ่อม';
$this->params['breadcrumbs'][] = ['label' => 'สถานะอุปกรณ์', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repair-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
