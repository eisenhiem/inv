<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Location */

$this->title = 'เพิ่มสถานที่ตั้งอุปกรณ์';
$this->params['breadcrumbs'][] = ['label' => 'สถานที่ตั้งอุปกรณ์', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
