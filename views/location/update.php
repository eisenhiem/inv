<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Location */

$this->title = 'แก้ไขสถานที่ตั้งอุปกรณ์: ' . $model->LOCATION_ID;
$this->params['breadcrumbs'][] = ['label' => 'สถานที่ตั้งอุปกรณ์', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->LOCATION_ID, 'url' => ['view', 'id' => $model->LOCATION_ID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
