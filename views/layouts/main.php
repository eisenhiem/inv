<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'ระบบซ่อมบำรุงอุปกรณ์',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'หน้าแรก', 'url' => ['/site/index']],
            ['label' => 'รายการอุปกรณ์', 'url' => ['/items/index']],
            ['label' => 'รายการซ่อมบำรุง',  'url' => Yii::$app->user->isGuest ?(['/repairs/index']):(['/repairs/admin'])],
            ['label' => 'รายการตรวจสอบอุปกรณ์', 'url' => Yii::$app->user->isGuest ?(['/check/index']):(['/check/admin'])],
            ['label' => 'ตั้งค่าระบบ','visible' => !Yii::$app->user->isGuest, 'items' => [
                ['label' => 'ประเภทอุปกรณ์', 'url' => ['/itemtype/index']],
                ['label' => 'ที่มา', 'url' => ['/source/index']],
                ['label' => 'ประเภทการซ่อม', 'url' => ['/repairtype/index']],
                ['label' => 'สถานที่ตั้งอุปกรณ์', 'url' => ['/location/index']],
                ['label' => 'สถานะอุปกรณ์', 'url' => ['/repairstatus/index']],
                ['label' => 'ผลการจำหน่าย', 'url' => ['/repairdischart/index']],
                ['label' => 'ชนิดผลการตรวจ', 'url' => ['/checkresult/index']],
                ['label' => 'รายชื่อผู้ตรวจสอบ', 'url' => ['/checkby/index']],
                ['label' => 'รายชื่อผู้ลงนาม', 'url' => ['/sign/index']],
            ]],
            
            Yii::$app->user->isGuest ? (
                ['label' => 'เข้าสู่ระบบ', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'ออกจากระบบ (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
