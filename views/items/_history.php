<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

$this->title = 'ประวัติการซ่อม:' . $model->ITEM_NAME .' '. $model->ITEM_NO;

?>
<div class="items-index">
<h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'REQUIRE_DATE',
                'headerOptions' => ['style' => 'width:100px'],
            ],
            'PROBLEM_CUASE:ntext',
            //'REPAIR_DATE',
            'FINISH_DATE',
            'REPAIR_RESULT:ntext',
            [
                'attribute'=>'REPAIR_DISCHART_ID',
                'label'=>'ผลการดำเนินการ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getDischartName();
                }
            ],
        ],
    ]); ?>
</div>