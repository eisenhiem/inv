<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ItemType;
use app\models\Location;

$locate = ArrayHelper::map(Location::find()->all(), 'LOCATION_ID', 'LOCATION_NAME');
$itemtype = ArrayHelper::map(ItemType::find()->all(), 'ITEM_TYPE_ID', 'ITEM_TYPE_NAME');
/* @var $this yii\web\View */
/* @var $model app\models\ItemsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

        <div class="row">
            <div class="col-lg-3">
                <?= $form->field($model, 'ITEM_NO') ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'ITEM_NAME') ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'ITEM_TYPE_ID')->dropDownList($itemtype, ['prompt'=>'เลือกประเภทวัสดุ'])  ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'LOCATION_ID')->dropDownList($locate, ['prompt'=>'เลือกตำแหน่งที่ตั้งวัสดุ']) ?>
            </div>
        </div>
    <?php // = $form->field($model, 'ITEM_ID') ?>


    <?php // = $form->field($model, 'RECEIVE_DATE') ?>

    <?php // echo $form->field($model, 'WARRANTY_EXPIRE') ?>

    <?php // echo $form->field($model, 'PRODUCT_PRICE') ?>

    <?php // echo $form->field($model, 'DEPRECIATION') ?>

    <?php // echo $form->field($model, 'QRCODE') ?>

    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('ยกเลิก', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
