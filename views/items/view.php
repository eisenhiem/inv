<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Items */

$this->title = $model->ITEM_ID;
$this->params['breadcrumbs'][] = ['label' => 'รายการอุปกรณ์', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="items-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p> 
        <div class="row">
        <div class="col-md-2">
        <?= Html::a('ย้าย', ['move', 'id' => $model->ITEM_ID], ['class' => 'btn btn-primary','style'=>'width:150px']) ?>
        </div>
        <div class="col-md-2">
        <?= Html::a('ประวัติการย้าย', ['transfer', 'id' => $model->ITEM_ID], ['class' => 'btn btn-info','style'=>'width:150px']) ?>
        </div>
        <div class="col-md-2">
        <?= Html::a('แจ้งซ่อม', ['repair', 'id' => $model->ITEM_ID], ['class' => 'btn btn-warning','style'=>'width:150px']) ?>
        </div>
        <div class="col-md-2">
        <?= Html::a('ประวัติการแจ้งซ่อม', ['history', 'id' => $model->ITEM_ID], ['class' => 'btn btn-info','style'=>'width:150px']) ?>
        </div>
        <div class="col-md-2">
        <?= Html::a('ตรวจสอบ', ['check', 'id' => $model->ITEM_ID], ['class' => 'btn btn-success','style'=>'width:150px']) ?>
        </div>
        <div class="col-md-2">
        <?= Html::a('ประวัติการตรวจสอบ', ['checklist', 'id' => $model->ITEM_ID], ['class' => 'btn btn-info','style'=>'width:150px']) ?>
        </div>
        </div>
    </p>
    <p>
        <div class="row">
        <div class="col-md-2">
        <?= Html::a('แก้ไข', ['update', 'id' => $model->ITEM_ID], ['class' => 'btn btn-primary','style'=>'width:150px']) ?>
        </div>
        <div class="col-md-2">
        <?= Html::a('พิมพ์ Label', ['printlabel', 'id' => $model->ITEM_ID], ['class' => 'btn btn-info','style'=>'width:150px']) ?>
        </div>
        <div class="col-md-2">
        <?= Html::a('Generate QR Code', ['genqr', 'id' => $model->ITEM_ID], ['class' => 'btn btn-success','style'=>'width:150px']) ?>
        </div>
        <div class="col-md-2">
        <?= Html::a('ลบ', ['delete', 'id' => $model->ITEM_ID], [
            'class' => 'btn btn-danger',
            'style'=>'width:150px',
            'data' => [
                'confirm' => 'คุณแน่ใจว่าต้องการลบรายการนี้ใช่หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
        </div>
        </div>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'format'=>'raw',
                'attribute'=>'PICTURE',
                'value'=>Html::img($model->photoViewer,['class'=>'img-thumbnail','style'=>'width:300px;'])
            ],
            'ITEM_ID',
            'ITEM_NO',
            'ITEM_NAME',
            [
                'attribute'=>'ITEM_TYPE_ID',
                'label'=>'ประเภทอุปกรณ์',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getItemtypeName();
                }
            ],
            [
                'attribute'=>'LOCATION_ID',
                'label'=>'ที่ตั้งอุปกรณ์',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getLocationName();
                }
            ],
            'INCHARGE',
            'RECEIVE_DATE',
            'WARRANTY_EXPIRE',
            [
                'attribute'=>'PRODUCT_PRICE',
                'format'=>'decimal'
            ],
            [
                'label'=>'ใช้งานมาแล้ว',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getUse();
                }
            ],
            'DEPRECIATION',
            [
                'attribute'=>'SOURCE_ID',
                'label'=>'แหล่งงบ',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getSourceName();
                }
            ],
            [
                'label' => 'QR code',
                'value' => '/inv/web/qrcode/code'.$model->ITEM_ID.'.png',
                'format' => ['image',['width'=>'200','height'=>'200']],
            ],
        ]
    ]) ?>
</div>
