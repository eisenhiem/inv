<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Items */

$this->title = 'ประวัติการตรวจอุปกรณ์: ' . $model->ITEM_NO;
$this->params['breadcrumbs'][] = ['label' => 'รายการอุปกรณ์', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ITEM_NO, 'url' => ['view', 'id' => $model->ITEM_ID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="items-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::a('พิมพ์ประวัติการตรวจอุปกรณ์', ['printcheck','id' => $model->ITEM_ID], ['class' => 'btn btn-info']) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'CHECK_ID',
            //'ITEM_ID',
            'CHECK_DATE',
            //'CHECK_RESULT_ID',
            [
                'attribute'=>'CHECK_RESULT_ID',
                'label'=>'ผลการตรวจสอบ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getResultName();
                }
            ],
            [
                'attribute'=>'CHECK_BY_ID',
                'label'=>'ผู้ตรวจสอบ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getCheckByName();
                }
            ],
            'COMMENT:ntext',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
