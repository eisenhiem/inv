<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ItemType;
use app\models\Location;
use app\models\Source;
use kartik\date\DatePicker;

$itemtype = ArrayHelper::map(ItemType::find()->all(), 'ITEM_TYPE_ID', 'ITEM_TYPE_NAME');
$locate = ArrayHelper::map(Location::find()->all(), 'LOCATION_ID', 'LOCATION_NAME');
$source = ArrayHelper::map(Source::find()->all(), 'SOURCE_ID', 'SOURCE_NAME');
/* @var $this yii\web\View */
/* @var $model app\models\Items */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="items-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>
    <div class="well text-center">
        <?= Html::img($model->getPhotoViewer(),['style'=>'width:100px;','class'=>'img-rounded']); ?>
    </div>
    <?= $form->field($model, 'PICTURE')->fileInput() ?>
    <div class="col-md-4">
    <?= $form->field($model, 'ITEM_NO')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4">
    <?= $form->field($model, 'ITEM_NAME')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4">
    <?= $form->field($model, 'ITEM_TYPE_ID')->dropDownList($itemtype, ['prompt'=>'เลือกประเภทวัสดุ']) ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'LOCATION_ID')->dropDownList($locate, ['prompt'=>'เลือกตำแหน่งที่ตั้งวัสดุ']) ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'INCHARGE')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'RECEIVE_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'RECEIVE_DATE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่รับวัสดุ'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'WARRANTY_EXPIRE')->widget(DatePicker::ClassName(),
    [
        'name' => 'WARRANTY_EXPIRE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่หมดประกัน'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); 
    ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'PRODUCT_PRICE')->textInput() ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'DEPRECIATION')->dropDownList(array_combine(range(1, 20), range(1, 20))) ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'SOURCE_ID')->dropDownList($source, ['prompt'=>'เลือกแหล่งงบ']) ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'STATUS')->radioList([1 => 'ใช้งาน', 0 => 'จำหน่าย'])->label('สถานะ'); ?>
    </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
