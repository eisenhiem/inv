<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Items */

$this->title = 'แก้ไขรายการ: ' . $model->ITEM_ID;
$this->params['breadcrumbs'][] = ['label' => 'รายการอุปกรณ์', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ITEM_ID, 'url' => ['view', 'id' => $model->ITEM_ID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="items-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
