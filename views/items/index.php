<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการอุปกรณ์';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="items-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('เพิ่มอุปกรณ์', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'before' => ''
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ITEM_ID',
            'ITEM_NO',
            'ITEM_NAME',
            [
                'attribute'=>'ITEM_TYPE_ID',
                'label'=>'ประเภทอุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getItemtypeName();
                }
            ],
            [
                'attribute'=>'LOCATION_ID',
                'label'=>'ที่ตั้งอุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getLocationName();
                }
            ],
            'INCHARGE',
            //'RECEIVE_DATE',
            //'WARRANTY_EXPIRE',
            //'PRODUCT_PRICE',
            //'DEPRECIATION',
            //'QRCODE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
