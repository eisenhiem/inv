<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RepairDischart */

$this->title = 'เพิ่มผลการจำหน่าย';
$this->params['breadcrumbs'][] = ['label' => 'ผลการจำหน่าย', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repair-dischart-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
