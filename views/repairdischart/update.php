<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RepairDischart */

$this->title = 'แก้ไขผลการจำหน่าย: ' . $model->REPAIR_DISCHART_ID;
$this->params['breadcrumbs'][] = ['label' => 'ผลการจำหน่าย', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->REPAIR_DISCHART_ID, 'url' => ['view', 'id' => $model->REPAIR_DISCHART_ID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repair-dischart-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
