<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RepairDischart */

$this->title = $model->REPAIR_DISCHART_ID;
$this->params['breadcrumbs'][] = ['label' => 'การจำหน่าย', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repair-dischart-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->REPAIR_DISCHART_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->REPAIR_DISCHART_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณแน่ใจว่าต้องการลบรายการนี้ใช่หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'REPAIR_DISCHART_ID',
            'REPAIR_DISCHART_NAME',
        ],
    ]) ?>

</div>
