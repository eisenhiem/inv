<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CheckResult */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-result-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CHECK_RESULT_ID')->textInput() ?>

    <?= $form->field($model, 'CHECK_RESULT_NAME')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
