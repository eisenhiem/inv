<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CheckResult */

$this->title = $model->CHECK_RESULT_ID;
$this->params['breadcrumbs'][] = ['label' => 'ผลการตรวจสอบ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-result-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->CHECK_RESULT_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->CHECK_RESULT_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'CHECK_RESULT_ID',
            'CHECK_RESULT_NAME',
        ],
    ]) ?>

</div>
