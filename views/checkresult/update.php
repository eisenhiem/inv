<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CheckResult */

$this->title = 'แก้ไขผลการตรวจสอบ: ' . $model->CHECK_RESULT_ID;
$this->params['breadcrumbs'][] = ['label' => 'ผลการตรวจสอบ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CHECK_RESULT_ID, 'url' => ['view', 'id' => $model->CHECK_RESULT_ID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-result-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
