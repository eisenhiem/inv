<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Move */

$this->title = 'Update Move: ' . $model->MOVE_ID;
$this->params['breadcrumbs'][] = ['label' => 'Moves', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->MOVE_ID, 'url' => ['view', 'id' => $model->MOVE_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="move-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
