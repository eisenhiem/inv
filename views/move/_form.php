<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Items;
use app\models\Location;
use kartik\date\DatePicker;

$locate = ArrayHelper::map(Location::find()->all(), 'LOCATION_ID', 'LOCATION_NAME');

/* @var $this yii\web\View */
/* @var $model app\models\Move */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="move-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ITEM_ID')->textInput(['value'=> $id,'maxlength' => true]) ?>

    <?= $form->field($model, 'MOVE_FORM_ID')->textInput(['value'=> $location,'maxlength' => true]) ?>

    <?= $form->field($model, 'MOVE_TO_ID')->dropDownList($locate, ['prompt'=>'เลือกตำแหน่งที่ตั้งวัสดุ']) ?>

    <?= $form->field($model, 'MOVE_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'MOVE_DATE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่ย้ายวัสดุ'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
