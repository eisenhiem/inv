<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RepairType */

$this->title = 'แก้ไขประเภทการซ่อม: ' . $model->REPAIR_TYPE_ID;
$this->params['breadcrumbs'][] = ['label' => 'ประเภทการซ่อม', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->REPAIR_TYPE_ID, 'url' => ['view', 'id' => $model->REPAIR_TYPE_ID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repair-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
