<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RepairType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="repair-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'REPAIR_TYPE_ID')->textInput() ?>

    <?= $form->field($model, 'REPAIR_TYPE_NAME')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
