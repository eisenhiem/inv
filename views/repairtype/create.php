<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RepairType */

$this->title = 'เพิ่มประเภทการซ่อม';
$this->params['breadcrumbs'][] = ['label' => 'ประเภทการซ่อม', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repair-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
