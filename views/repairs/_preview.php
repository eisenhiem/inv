<?php
use yii\helpers\Html;

function changeDate($date){
//ใช้ Function explode ในการแยกไฟล์ ออกเป็น  Array
  if(is_null($date)){
    return '...............................';
  } else {
    $get_date = explode("-",$date);
    //กำหนดชื่อเดือนใส่ตัวแปร $month
    $month = array("01"=>"มกราคม","02"=>"กุมภาพันธ์","03"=>"มีนาคม","04"=>"เมษายน","05"=>"พฤษภาคม","06"=>"มิถุนายน","07"=>"กรกฎาคม","08"=>"สิงหาคม","09"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
    //month
    $get_month = $get_date["1"];
    //year    
    $year = $get_date["0"]+543;
    return $get_date["2"]." ".$month[$get_month]." พ.ศ. ".$year;
  }
}

function checkRequire($data){
  if(is_null($data)){
    return '..............................................';
  } else {
    return $data;
  }
}
//การเรียกใช้งาน Function

?>

<table width="100%">
    <tr>
        <td width="260">
            <?=Html::img(Yii::getAlias('@app').'/web/img/crut.png', ['width' => 60])?>
        </td>
        <td valign="bottom"><h2>บันทึกข้อความ</h2><br>
        </td>
    </tr>
</table>
<table width="100%">
    <tr><td colspan=2><br><br></td>
    </tr>
    <tr>
      <td colspan=2>
      <p>
      ส่วนราชการ กลุ่มงานการจัดการทั่วไป <?php echo $data->HOSPITAL_NAME?><br><br>
      ที่ <?php echo $data->HOSP_NO?> / &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;วันที่ <?php echo changeDate($model->REQUIRE_DATE) ?><br><br>
      เรื่อง ขอส่งซ่อมอุปกรณ์ <?php echo $model->getItemName()?>
      </p>
    </td>
    </tr>
    <tr>
    <td colspan=2><hr>
    </td>
    </tr>    
    <tr>
    <td colspan=2>เรียน ผู้อำนวยการ<?php echo $data->HOSPITAL_NAME?>
    </td>
    </tr>
  <tr>
        <td colspan=2><br><br>
        <p> &emsp; &emsp; &emsp; เนื่องด้วยกลุ่มงาน/ฝ่าย ........................................................ มีความประสงค์ที่จะส่งซ่อมบำรุง อุปกรณ์ <?php echo $model->getItemName().' เลขที่อุปกรณ์ '.$model->getItemNo()?> เนื่องจากพบปัญหา 
        <?php echo $model->PROBLEM_CUASE ?> 
        </p>
        </td>
    </tr>
    <tr>
    <td colspan=2><br></td>
    </tr>
    <tr>
        <td colspan=2>
        <p> &emsp; &emsp; &emsp; จึงเรียนมาเพื่อพิจารณาดำเดินการ</p>
        </td>
    </tr>
    <tr>
    <td colspan=2><br></td>
    </tr>
    <tr>
        <td width="340">
        </td>
        <td>(<?php echo checkRequire($model->REQUIRE_NAME)?>)</td>
    </tr>
</table>
<br><br>
<table class="table_bordered" width="100%" border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td width="50%">
          <h4>ผลการตรวจสอบ</h4><br>
          <p>
          &emsp; &emsp; &emsp; พบว่า<?php echo $model->REPAIR_RESULT."<br>" ?><br><br>
          </p>
          <p>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; (<?php echo $data->IT_NAME ?>)<br>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; <?php echo $data->IT_POSITION ?> <br>
          </p>
          <br>
        </td>
        <td width="50%">
          <h4>ผลการดำเนินงานซ่อม</h4><br>
          <p>
          &emsp; &emsp; &emsp; ดำเนินการโดย <?php echo $model->getDischartName()."<br>" ?><br><br>
          </p>
          <p>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; (<?php echo $model->getRepairByName() ?>)<br>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; <?php echo $data->IT_POSITION ?> <br>
          </p>
        </td>
    </tr>
    <tr>
        <td width="55%">
          <h4>เรียนผู้อำนวยการ<?php echo $data->HOSPITAL_NAME?></h4><br>
          <p>
          &emsp;[&emsp;] เห็นควร <br>
          &emsp;[&emsp;] ไม่เห็นควร&emsp;ให้ดำเนินการตามที่เสนอ <br><br><br>
          </p>
          <p>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp;(<?php echo $data->MANAGE_NAME ?>) <br>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;<?php echo $data->MANAGE_POSITION ?> <br><br>
          </p>
        </td>
        <td width="45%">
          <h4>ส่งมอบงาน</h4><br>
          <p>
          &emsp; &emsp; &emsp; อุปกรณ์ <?php echo $model->getItemName() ?> ได้ทำการซ่อมบำรุงเสร็จสิ้น ได้ส่งมอบอุปกรณ์คืนแก่กลุ่มงานที่รับผิดชอบ<br> 
          เมื่อวันที่ .......................... <br><br>
          </p>
          <p>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; ผู้รับมอบ<br>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; ( &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; )<br><br>
          </p>
          <br>
        </td>
    </tr>
    <tr>
        <td width="50%">
          <h4>ทราบ</h4><br>
          <p>
          &emsp; [&emsp;] ให้ดำเนินการได้ <br> &emsp; [&emsp;] ไม่ควรดำเนินการเนื่องจาก .....................................<br><br>
          ........................................................................................ <br><br><br>
          </p>
          <p>
          &emsp; &emsp; &emsp;&emsp;&emsp;&emsp;(<?php echo $data->DIRECTOR_NAME ?>)<br>
          &ensp; &emsp; &emsp;&emsp;&emsp; &emsp; &emsp;<?php echo $data->DIRECTOR_POSITION1 ?><br>
          &emsp; &emsp; &emsp;&emsp;<?php echo $data->DIRECTOR_POSITION2 ?>
          </p>
          <br><br>
        </td>
        <td width="50%">
          <h4>&emsp; &emsp;จึงเรียนมาเพื่อโปรดทราบ</h4><br><br>
          <p>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; (<?php echo $data->IT_NAME ?>)<br>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; <?php echo $data->IT_POSITION ?> <br>
          </p>
          <br><br>
          <h4>&emsp; &emsp;ทราบ</h4><br><br>
          <p>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp;(<?php echo $data->MANAGE_NAME ?>) <br>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;<?php echo $data->MANAGE_POSITION ?>  <br><br>
          </p>
        </td>
    </tr>
</table>
