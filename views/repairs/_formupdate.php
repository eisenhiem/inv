<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use app\models\RepairType;
use app\models\RepairDischart;
use app\models\CheckBy;
use app\models\RepairStatus;

$repairby = ArrayHelper::map(CheckBy::find()->all(), 'CHECK_BY_ID', 'CHECK_BY_NAME');
$repairtype = ArrayHelper::map(RepairType::find()->all(), 'REPAIR_TYPE_ID', 'REPAIR_TYPE_NAME');
$dctype = ArrayHelper::map(RepairDischart::find()->all(), 'REPAIR_DISCHART_ID', 'REPAIR_DISCHART_NAME');
$repairstatus = ArrayHelper::map(RepairStatus::find()->all(), 'REPAIR_STATUS_ID', 'REPAIR_STATUS_NAME');

/* @var $this yii\web\View */
/* @var $model app\models\repairs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="repairs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ITEM_ID')->hiddenInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'REQUIRE_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'REQUIRE_DATE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่แจ้งซ่อม'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'PROBLEM_CUASE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'REQUIRE_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'REPAIR_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'REPAIR_DATE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่ดำเนินการ'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'QUARANTINE_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'QUARANTINE_DATE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่คาดว่าจะเสร็จ'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'FINISH_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'FINISH_DATE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่เสร็จงาน'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'REPAIR_RESULT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'REPAIR_TYPE_ID')->dropDownList($repairtype, ['prompt'=>'เลือกประเภทการซ่อม']) ?>

    <?= $form->field($model, 'REPAIR_STATUS_ID')->dropDownList($repairstatus, ['prompt'=>'เลือกสถานะการซ่อม']) ?>

    <?= $form->field($model, 'REPAIR_DISCHART_ID')->dropDownList($dctype, ['prompt'=>'เลือกการจำหน่าย']) ?>

    <?= $form->field($model, 'CHECK_BY_ID')->dropDownList($repairby, ['prompt'=>'เลือกผู้ซ่อม']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
