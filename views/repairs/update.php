<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\repairs */

$this->title = 'แก้ไขรายการซ่อม: ' . $model->REPAIR_ID;
$this->params['breadcrumbs'][] = ['label' => 'รายการซ่อม', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->REPAIR_ID, 'url' => ['view', 'id' => $model->REPAIR_ID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repairs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formupdate', [
        'model' => $model,
    ]) ?>

</div>
