<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\repairsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="repairs-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
        <div class="row">
            <div class="col-lg-3">
            </div>
            <div class="col-lg-3">
            </div>
            <div class="col-lg-3">
            </div>
            <div class="col-lg-3">
            </div>
        </div>

    <?= $form->field($model, 'REPAIR_ID') ?>

    <?= $form->field($model, 'ITEM_ID') ?>

    <?= $form->field($model, 'REQUIRE_DATE') ?>

    <?= $form->field($model, 'PROBLEM_CUASE') ?>

    <?= $form->field($model, 'REPAIR_DATE') ?>

    <?php // echo $form->field($model, 'FINISH_DATE') ?>

    <?php // echo $form->field($model, 'REPAIR_RESULT') ?>

    <?php // echo $form->field($model, 'REPAIR_TYPE_ID') ?>

    <?php // echo $form->field($model, 'REPAIR_DISCHART_ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
