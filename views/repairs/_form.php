<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use app\models\RepairType;
use app\models\RepairDischart;
use app\models\CheckBy;

$repairby = ArrayHelper::map(CheckBy::find()->all(), 'CHECK_BY_ID', 'CHECK_BY_ID');
$repairtype = ArrayHelper::map(RepairType::find()->all(), 'REPAIR_TYPE_ID', 'REPAIR_TYPE_NAME');
$dctype = ArrayHelper::map(RepairDischart::find()->all(), 'REPAIR_DISCHART_ID', 'REPAIR_DISCHART_NAME');

/* @var $this yii\web\View */
/* @var $model app\models\repairs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="repairs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ITEM_ID')->hiddenInput(['value'=> $item_id,'maxlength' => true]) ?>

    <?= $form->field($model, 'REQUIRE_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'REQUIRE_DATE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่แจ้งซ่อม'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'PROBLEM_CUASE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'REQUIRE_NAME')->textInput(['maxlength' => true]) ?>
<?php 
/* not use for create repair 
    <?= $form->field($model, 'REPAIR_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'REPAIR_DATE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่ดำเนินการ'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'FINISH_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'FINISH_DATE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่เสร็จงาน'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'REPAIR_RESULT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'REPAIR_TYPE_ID')->dropDownList($repairtype, ['prompt'=>'เลือกประเภทการซ่อม']) ?>

    <?= $form->field($model, 'REPAIR_DISCHART_ID')->dropDownList($dctype, ['prompt'=>'เลือกการจำหน่าย']) ?>
*/
?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
