<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\repairs */

$this->title = $model->REPAIR_ID;
$this->params['breadcrumbs'][] = ['label' => 'รายการซ่อม', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repairs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->REPAIR_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('พิมพ์ใบส่งซ่อมอุปกรณ์', ['pdf', 'id' => $model->REPAIR_ID], ['class' => 'btn btn-info']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->REPAIR_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณแน่ใจว่าต้องการลบรายการนี้ใช่หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'REPAIR_ID',
            [
                'attribute'=>'ITEM_ID',
                'label'=>'เลขที่อุปกรณ์',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getItemNo();
                }
            ],
            [
                'attribute'=>'ITEM_ID',
                'label'=>'ชื่ออุปกรณ์',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getItemName();
                }
            ],
            'REQUIRE_DATE',
            'PROBLEM_CUASE:ntext',
            'REQUIRE_NAME',
            'REPAIR_DATE',
            'QUARANTINE_DATE',
            'FINISH_DATE',
            'REPAIR_RESULT:ntext',
            [
                'attribute'=>'REPAIR_TYPE_ID',
                'label'=>'ชื่ออุปกรณ์',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getRepairtypeName();
                }
            ],
            [
                'attribute'=>'REPAIR_STATUS_ID',
                'label'=>'สถานะส่งซ่อมอุปกรณ์',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getRepairStatusName();
                }
            ],
            [
                'attribute'=>'REPAIR_DISCHART_ID',
                'label'=>'ชื่ออุปกรณ์',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getDischartName();
                }
            ],
            [
                'attribute'=>'CHECK_BY_ID',
                'label'=>'ผู้ซ่อม',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getRepairByName();
                }
            ],
        ],
    ]) ?>

</div>
