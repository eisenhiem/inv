<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\repairsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการซ่อม';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repairs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('เพิ่มรายการซ่อม', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'before' => ''
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'ITEM_ID',
                'label'=>'เลขที่อุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getItemNo();
                }
            ],
            [
                'attribute'=>'ITEM_ID',
                'label'=>'ชื่ออุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getItemName();
                }
            ],
            'REQUIRE_DATE',
            'PROBLEM_CUASE:ntext',
            'REQUIRE_NAME',
            'QUARANTINE_DATE',
            'REPAIR_RESULT:ntext',
            [
                'attribute'=>'REPAIR_STATUS_ID',
                'label'=>'สถานะอุปกรณ์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getRepairStatusName();
                }
            ],
            [
                'attribute'=>'REPAIR_DISCHART_ID',
                'label'=>'ผลการดำเนินการ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getDischartName();
                }
            ],
            //'FINISH_DATE',
            //'REPAIR_TYPE_ID',
            //'REPAIR_DISCHART_ID',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
