<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ItemType */

$this->title = 'แก้ไขประเภทอุปกรณ์: ' . $model->ITEM_TYPE_ID;
$this->params['breadcrumbs'][] = ['label' => 'ประเภทอุปกรณ์', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ITEM_TYPE_ID, 'url' => ['view', 'id' => $model->ITEM_TYPE_ID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
