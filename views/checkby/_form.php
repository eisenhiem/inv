<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CheckBy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-by-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CHECK_BY_NAME')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
