<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CheckBy */

$this->title = 'เพิ่มผู้ตรวจสอบ';
$this->params['breadcrumbs'][] = ['label' => 'รายชื่อผู้ตรวจสอบ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-by-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
