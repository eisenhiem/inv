<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CheckBy */

$this->title = 'แก้ไขผู้ตรวจสอบ: ' . $model->CHECK_BY_NAME;
$this->params['breadcrumbs'][] = ['label' => 'รายชื่อผู้ตรวจสอบ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CHECK_BY_NAME, 'url' => ['view', 'id' => $model->CHECK_BY_ID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-by-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
