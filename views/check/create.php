<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Check */

$this->title = 'เพิ่มรายการตรวจสอบ';
$this->params['breadcrumbs'][] = ['label' => 'รายการตรวจสอบ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,'item_id' => $item_id
    ]) ?>

</div>
