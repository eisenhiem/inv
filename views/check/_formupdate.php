<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\CheckResult;
use app\models\CheckBy;
use kartik\date\DatePicker;

$chkresult = ArrayHelper::map(CheckResult::find()->all(), 'CHECK_RESULT_ID', 'CHECK_RESULT_NAME');
$chkby = ArrayHelper::map(CheckBy::find()->all(), 'CHECK_BY_ID', 'CHECK_BY_NAME');
/* @var $this yii\web\View */
/* @var $model app\models\ItemCheck */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-check-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ITEM_ID')->hiddenInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CHECK_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'CHECK_DATE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่ตรวจสอบ'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd hh:ii:ss',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'CHECK_RESULT_ID')->dropDownList($chkresult, ['prompt'=>'เลือกผลการตรวจสอบ'])  ?>

    <?= $form->field($model, 'COMMENT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'CHECK_BY_ID')->dropDownList($chkby, ['prompt'=>'เลือกผู้ตรวจสอบ']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
