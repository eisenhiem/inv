<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Check */

$this->title = 'ปรับปรุงรายการตรวจสอบ: ' . $model->CHECK_ID;
$this->params['breadcrumbs'][] = ['label' => 'Checks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CHECK_ID, 'url' => ['view', 'id' => $model->CHECK_ID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formupdate', [
        'model' => $model,
    ]) ?>

</div>
