<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Check */

$this->title = $model->CHECK_ID;
$this->params['breadcrumbs'][] = ['label' => 'รายการตรวจสอบ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->CHECK_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->CHECK_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณแน่ใจว่าต้องการลบรายการนี้ใช่หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'CHECK_ID',
            'ITEM_ID',
            [
                'attribute'=>'ITEM_ID',
                'label'=>'เลขที่อุปกรณ์',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getItemNo();
                }
            ],
            [
                'attribute'=>'ITEM_ID',
                'label'=>'ชื่ออุปกรณ์',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getItemName();
                }
            ],
            'CHECK_DATE',
            //'CHECK_RESULT_ID',
            [
                'attribute'=>'CHECK_RESULT_ID',
                'label'=>'ผลการตรวจสอบ',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getResultName();
                }
            ],
            'COMMENT:ntext',
            [
                'attribute'=>'CHECK_BY_ID',
                'label'=>'ผู้ตรวจสอบ',
                'format'=>'text',//raw, html
                'value'=>function($data){
                    return $data->getCheckByName();
                }
            ],
        ],
    ]) ?>

</div>
