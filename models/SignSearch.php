<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sign;

/**
 * SignSearch represents the model behind the search form of `app\models\Sign`.
 */
class SignSearch extends Sign
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['HOSPITAL_NAME','HOSP_NO','DIRECTOR_NAME', 'DIRECTOR_POSITION1', 'DIRECTOR_POSITION2', 'MANAGE_NAME', 'MANAGE_POSITION', 'IT_NAME', 'IT_POSITION'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
        ]);

        $query->andFilterWhere(['like', 'DIRECTOR_NAME', $this->DIRECTOR_NAME])
            ->andFilterWhere(['like', 'DIRECTOR_POSITION1', $this->DIRECTOR_POSITION1])
            ->andFilterWhere(['like', 'DIRECTOR_POSITION2', $this->DIRECTOR_POSITION2])
            ->andFilterWhere(['like', 'MANAGE_NAME', $this->MANAGE_NAME])
            ->andFilterWhere(['like', 'MANAGE_POSITION', $this->MANAGE_POSITION])
            ->andFilterWhere(['like', 'IT_NAME', $this->IT_NAME])
            ->andFilterWhere(['like', 'IT_POSITION', $this->IT_POSITION])
            ->andFilterWhere(['like', 'HOSPITAL_NAME', $this->HOSPITAL_NAME])
            ->andFilterWhere(['like', 'HOSP_NO', $this->HOSP_NO]);

        return $dataProvider;
    }
}
