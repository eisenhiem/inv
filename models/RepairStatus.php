<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "repair_status".
 *
 * @property int $REPAIR_STATUS_ID
 * @property string $REPAIR_STATUS_NAME
 */
class RepairStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'repair_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['REPAIR_STATUS_NAME'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'REPAIR_STATUS_ID' => 'Repair Status ID',
            'REPAIR_STATUS_NAME' => 'Repair Status Name',
        ];
    }
}
