<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sign".
 *
 * @property int $ID
 * @property string $DIRECTOR_NAME
 * @property string $DIRECTOR_POSITION1
 * @property string $DIRECTOR_POSITION2
 * @property string $MANAGE_NAME
 * @property string $MANAGE_POSITION
 * @property string $IT_NAME
 * @property string $IT_POSITION
 * @property string $HOSPITAL_NAME
 * @property string $HOSP_NO
 */
class Sign extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sign';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DIRECTOR_NAME', 'MANAGE_NAME','HOSPITAL_NAME', 'IT_NAME','DIRECTOR_POSITION1'], 'string', 'max' => 100],
            [['DIRECTOR_POSITION2'], 'string', 'max' => 50],
            [['MANAGE_POSITION', 'IT_POSITION', 'HOSP_NO'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'DIRECTOR_NAME' => 'ชื่อผู้อำนวยการ',
            'DIRECTOR_POSITION1' => 'ตำแหน่ง 1',
            'DIRECTOR_POSITION2' => 'ตำแหน่ง 2',
            'MANAGE_NAME' => 'ชื่อหัวหน้ากลุ่มงาน',
            'MANAGE_POSITION' => 'ตำแหน่งหัวหน้ากลุ่มงาน',
            'IT_NAME' => 'ชื่อเจ้าหน้าที่',
            'IT_POSITION' => 'ตำแหน่งเจ้าหน้าที่',
            'HOSPITAL_NAME' => 'ชื่อโรงพยาบาล',
            'HOSP_NO' => 'เลขที่หนังสือ',
        ];
    }
}
