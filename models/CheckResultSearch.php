<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CheckResult;

/**
 * CheckResultSearch represents the model behind the search form of `app\models\CheckResult`.
 */
class CheckResultSearch extends CheckResult
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CHECK_RESULT_ID'], 'integer'],
            [['CHECK_RESULT_NAME'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CheckResult::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'CHECK_RESULT_ID' => $this->CHECK_RESULT_ID,
        ]);

        $query->andFilterWhere(['like', 'CHECK_RESULT_NAME', $this->CHECK_RESULT_NAME]);

        return $dataProvider;
    }
}
