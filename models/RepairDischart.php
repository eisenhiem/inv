<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "repair_dischart".
 *
 * @property int $REPAIR_DISCHART_ID
 * @property string $REPAIR_DISCHART_NAME
 */
class RepairDischart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'repair_dischart';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['REPAIR_DISCHART_ID'], 'required'],
            [['REPAIR_DISCHART_ID'], 'integer'],
            [['REPAIR_DISCHART_NAME'], 'string', 'max' => 255],
            [['REPAIR_DISCHART_ID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'REPAIR_DISCHART_ID' => 'Repair  Dischart  ID',
            'REPAIR_DISCHART_NAME' => 'ประเภทการจำหน่าย',
        ];
    }
}
