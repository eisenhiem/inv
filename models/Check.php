<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "check".
 *
 * @property string $CHECK_ID
 * @property string $ITEM_ID
 * @property string $CHECK_DATE
 * @property int $CHECK_RESULT_ID
 * @property int $CHECK_BY_ID
 * @property string $COMMENT
 *
 * @property CheckResult $cHECKRESULT
 * @property Items $iTEM
 */
class Check extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'check';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ITEM_ID'], 'required'],
            [['ITEM_ID', 'CHECK_RESULT_ID','CHECK_BY_ID'], 'integer'],
            [['CHECK_DATE'], 'safe'],
            [['COMMENT'], 'string'],
            [['CHECK_RESULT_ID'], 'exist', 'skipOnError' => true, 'targetClass' => CheckResult::className(), 'targetAttribute' => ['CHECK_RESULT_ID' => 'CHECK_RESULT_ID']],
            [['CHECK_BY_ID'], 'exist', 'skipOnError' => true, 'targetClass' => CheckBy::className(), 'targetAttribute' => ['CHECK_BY_ID' => 'CHECK_BY_ID']],
            [['ITEM_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['ITEM_ID' => 'ITEM_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CHECK_ID' => 'รหัสการตรวจสอบ',
            'ITEM_ID' => 'รหัสอุปกรณ์',
            'CHECK_DATE' => 'วันที่ตรวจสอบ',
            'CHECK_RESULT_ID' => 'ผลการตรวจสอบ',
            'COMMENT' => 'หมายเหตุ',
            'CHECK_BY_ID' => 'ผู้ตรวจสอบ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResult()
    {
        return $this->hasOne(CheckResult::className(), ['CHECK_RESULT_ID' => 'CHECK_RESULT_ID']);
    }

    public function getResultName(){
        $model=$this->result;
        return $model?$model->CHECK_RESULT_NAME:'';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['ITEM_ID' => 'ITEM_ID']);
    }
        
    public function getItemNo(){
        $model=$this->item;
        return $model?$model->ITEM_NO:'';
    }

    public function getItemName(){
        $model=$this->item;
        return $model?$model->ITEM_NAME:'';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckby()
    {
        return $this->hasOne(CheckBy::className(), ['CHECK_BY_ID' => 'CHECK_BY_ID']);
    }

    public function getCheckByName(){
        $model=$this->checkby;
        return $model?$model->CHECK_BY_NAME:'';
    }

}
