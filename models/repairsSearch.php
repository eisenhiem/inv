<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Repairs;

/**
 * RepairsSearch represents the model behind the search form of `app\models\Repairs`.
 */
class RepairsSearch extends Repairs
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['REPAIR_ID', 'ITEM_ID', 'REPAIR_TYPE_ID', 'REPAIR_DISCHART_ID', 'CHECK_BY_ID','REPAIR_STATUS_ID'], 'integer'],
            [['REQUIRE_DATE', 'PROBLEM_CUASE', 'REPAIR_DATE', 'FINISH_DATE', 'REPAIR_RESULT','REQUIRE_NAME','QUARANTINE_DATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Repairs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'REPAIR_ID' => $this->REPAIR_ID,
            'ITEM_ID' => $this->ITEM_ID,
            'REQUIRE_DATE' => $this->REQUIRE_DATE,
            'REPAIR_DATE' => $this->REPAIR_DATE,
            'QUARANTINE_DATE' => $this->FINISH_DATE,
            'FINISH_DATE' => $this->FINISH_DATE,
            'REPAIR_TYPE_ID' => $this->REPAIR_TYPE_ID,
            'REPAIR_STATUS_ID' => $this->REPAIR_TYPE_ID,
            'REPAIR_DISCHART_ID' => $this->REPAIR_DISCHART_ID,
            'CHECK_BY_ID' => $this->CHECK_BY_ID,
        ]);

        $query->andFilterWhere(['like', 'PROBLEM_CUASE', $this->PROBLEM_CUASE])
            ->andFilterWhere(['like', 'REQUIRE_NAME', $this->REQUIRE_NAME])
            ->andFilterWhere(['like', 'REPAIR_RESULT', $this->REPAIR_RESULT]);

        return $dataProvider;
    }
}
