<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Items;

/**
 * ItemsSearch represents the model behind the search form of `app\models\Items`.
 */
class ItemsSearch extends Items
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ITEM_ID', 'ITEM_TYPE_ID', 'LOCATION_ID', 'SOURCE_ID'], 'integer'],
            [['ITEM_NO', 'ITEM_NAME', 'INCHARGE', 'RECEIVE_DATE', 'WARRANTY_EXPIRE','PICTURE'], 'safe'],
            [['PRODUCT_PRICE', 'DEPRECIATION'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Items::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ITEM_ID' => $this->ITEM_ID,
            'ITEM_TYPE_ID' => $this->ITEM_TYPE_ID,
            'RECEIVE_DATE' => $this->RECEIVE_DATE,
            'WARRANTY_EXPIRE' => $this->WARRANTY_EXPIRE,
            'PRODUCT_PRICE' => $this->PRODUCT_PRICE,
            'DEPRECIATION' => $this->DEPRECIATION,
            'LOCATION_ID' => $this->LOCATION_ID, 
            'SOURCE_ID' => $this->LOCATION_ID, 
        ]);

        $query->andFilterWhere(['like', 'ITEM_NO', $this->ITEM_NO])
            ->andFilterWhere(['like', 'ITEM_NAME', $this->ITEM_NAME])
            ->andFilterWhere(['like', 'INCHARGE', $this->INCHARGE])
            ->andFilterWhere(['like', 'PICTURE', $this->PICTURE]);

        return $dataProvider;
    }
}
