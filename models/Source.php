<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "source".
 *
 * @property string $source_id
 * @property string $source_name
 */
class Source extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'source';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SOURCE_NAME'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SOURCE_ID' => 'Source ID',
            'SOURCE_NAME' => 'แหล่งงบ',
        ];
    }
}
